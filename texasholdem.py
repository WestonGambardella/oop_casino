import itertools
import random

minimum_bet = 15


class DeckOfCards:
    def __init__(self):
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
        suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        self.deck = [{'rank': rank, 'suit': suit} for rank, suit in itertools.product(ranks, suits)]

class Dealer:
    def __init__(self, deck):
        random.shuffle(deck.deck)
        self.shuffled_deck = deck.deck

class Player:
    def __init__(self, name, buy_in):
        if not isinstance(name, str):
            raise ValueError("name must be in string form")
        elif not isinstance(buy_in, int):
            raise ValueError("buy in must be an integer")
        elif buy_in <= 0:
            raise ValueError("buy in must be a positive integer")
        else:
            self.name = name
            self.buy_in = buy_in

    def add_to_buy_in(self, amount):
        if not isinstance(amount, int):
            raise ValueError("amount must be an integer")
        if amount <= 0:
            raise ValueError("amount must be positive integer")
        self.buy_in = self.buy_in + amount

class Holdem:
    def __init__(self, minimum_bet, dealer):
        self.dealer = dealer
        self.big_blind_counter = 0
        self.small_blind_counter = 1
        self.minimum_bet = minimum_bet
        self.players = {}
        self.player_list = []
        self.pot = 0
        self.current_bet = 0
        self.current_game_cards = {}

    
    def deal_one_card_to_players(self):
        for player, items in self.players.items():
            new_card = self.dealer.shuffled_deck[0]
            rank = new_card['rank']
            self.players[player]['cards'][rank] = new_card['suit']
            self.dealer.shuffled_deck.pop(0)

    def deal_flop(self):
        first_card = self.dealer.shuffled_deck[0]
        second_card = self.dealer.shuffled_deck[1]
        third_card = self.dealer.shuffled_deck[2]
        self.current_game_cards[first_card['rank']] = first_card['suit']
        self.current_game_cards[second_card['rank']] = second_card['suit']
        self.current_game_cards[third_card['rank']] = third_card['suit']
        self.dealer.shuffled_deck.pop(0)
        self.dealer.shuffled_deck.pop(1)
        self.dealer.shuffled_deck.pop(2)

    def deal_turn(self):
        turn_card = self.dealer.shuffled_deck[0]
        self.current_game_cards[turn_card['rank']] = turn_card['suit']
        self.dealer.shuffled_deck.pop(0)

    def deal_river(self):
        river_card = self.dealer.shuffled_deck[0]
        self.current_game_cards[river_card['rank']] = river_card['suit']
        self.dealer.shuffled_deck.pop(0)

    def add_player(self, player):
        self.players[player.name] = {"total": player.buy_in,
                                     "cards": {},
                                     "status": "playing"
                                     } 
    
    def check_players_total(self):
        for player, amount in self.players.items():
            if amount['total'] < self.minimum_bet:
                self.players[player]["status"] = "not playing"
                print(f"{player} did not have enough to start.")
    
    def distribute_blinds(self):
        big_blind = self.minimum_bet
        small_blind = (big_blind/2)
        self.pot += big_blind
        self.pot += small_blind
        for player in self.players.keys():
            self.player_list.append(player)
        if len(self.player_list) < 3:
            if self.big_blind_counter == 0:
                new_total_small = self.players[self.player_list[1]]['total'] - small_blind
                self.players[self.player_list[1]]['total'] = new_total_small
                new_total_big = self.players[self.player_list[0]]['total'] - big_blind
                self.players[self.player_list[self.big_blind_counter]]['total'] = new_total_big
                self.big_blind_counter = (self.big_blind_counter + 1)
            else:
                new_total_small = self.players[self.player_list[0]]['total'] - small_blind
                self.players[self.player_list[0]]['total'] = new_total_small
                new_total_big = self.players[self.player_list[1]]['total'] - big_blind
                self.players[self.player_list[self.big_blind_counter]]['total'] = new_total_big
                self.big_blind_counter = 0
        else:
            if len(self.player_list) > self.big_blind_counter:
                big_blind_holder = self.player_list[self.big_blind_counter]
                new_total_big = self.players[big_blind_holder]['total'] - big_blind
                self.players[self.player_list[self.big_blind_counter]]['total'] = new_total_big
                self.big_blind_counter = (self.big_blind_counter + 1)
                if len(self.player_list) > self.small_blind_counter: 
                    small_blind_holder = self.player_list[self.small_blind_counter]
                    new_total_small = self.players[small_blind_holder]['total'] - small_blind
                    self.players[self.player_list[self.small_blind_counter]]['total'] = new_total_small
                    self.small_blind_counter = (self.small_blind_counter + 1)
                else:
                    small_blind_holder = self.player_list[0]
                    new_total_small = self.players[small_blind_holder]['total'] - small_blind
                    self.players[self.player_list[0]]['total'] = new_total_small
            else:
                big_blind_holder = self.player_list[0]
                new_total_big = self.players[big_blind_holder]['total'] - big_blind
                self.players[self.player_list[0]]['total'] = new_total_big
                self.big_blind_counter = 0
                new_total_small = self.players[self.player_list[1]]['total'] - small_blind
                self.players[self.player_list[1]]['total'] = new_total_small
                self.small_blind_counter = 1

    @staticmethod
    def call(player, current_bet):
        new_total = player["total"] - current_bet
        player["total"] = new_total
        print(f'You now have ${player["total"]}.')
        return player

    @staticmethod
    def bet(player):
        while True:
            bet = input("how much do you want to bet? ")
            try:
                bet = int(bet)
                if bet < minimum_bet:
                    print("Bet must be more than the minimum.")
                elif bet > player["total"]:
                    print("You can't bet more than you have!")
                else:
                    new_total = player["total"] - bet
                    player["total"] = new_total
                    print(f'You now have ${player["total"]}.')
                    return player
            except ValueError:
                print("Invalid input. Please enter a valid integer.")
                
    def turn(self):
        for player, items in self.players.items():
            before_total = items['total']
            while True:
                if self.players[player]["status"] == "playing":
                    choice = input(f"{player}, do you want to call, raise, or fold? Current bet is ${self.current_bet} ")
                    try:
                        if choice == 'raise':
                            after_bet = Holdem.bet(self.players[player])
                            diff = before_total - (after_bet['total'])
                            self.current_bet = diff
                            self.pot += diff
                            self.players[player] = after_bet
                            break
                        elif choice == 'call':
                            if self.current_bet == 0:
                                break
                            after_call = Holdem.call(self.players[player], self.current_bet)
                            diff = before_total - (after_call['total'])
                            self.current_bet = diff
                            self.pot += diff
                            self.players[player] = after_bet
                            break
                        elif choice == 'fold':
                            self.players[player]["status"] = "not playing"
                            print(f'{player} has folded.')
                            break
                        raise ValueError("Invalid input. Please enter call, raise, or fold.")
                    except ValueError as e:
                        print(e)
                break
        print(f"Pot is now {self.pot}")

deck = DeckOfCards()
dealer = Dealer(deck)
player1 = Player("player 1", 1000)
player2 = Player("player 2", 100)
player3 = Player("player 3", 10)
# player4 = Player("player 4", 300000)
game = Holdem(minimum_bet, dealer)
game.add_player(player1)
game.add_player(player2)
game.add_player(player3)
# game.add_player(player4)
game.distribute_blinds()
game.deal_one_card_to_players()
game.deal_one_card_to_players()
# print(game.players)
# print(game.pot)
# print(game.players)
# print(game.pot)
game.deal_flop()
print(game.players)
game.check_players_total()
print(game.players)
game.turn()

# game.distribute_blinds()
# print(game.players)
# game.distribute_blinds()
# print(game.players)
# game.distribute_blinds()
# print(game.players)