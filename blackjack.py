import itertools
import random



MINIMUM_BET = 5

class DeckOfCards:
    def __init__(self):
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
        suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        self.deck = [{'rank': rank, 'suit': suit} for rank, suit in itertools.product(ranks, suits)]

class Dealer:
    def __init__(self, deck):
        random.shuffle(deck.deck)
        self.shuffled_deck = deck.deck

class Player:
    count = 0
    def __init__(self, name, buy_in):
        if not isinstance(name, str):
            raise ValueError("name must be in string form")
        elif not isinstance(buy_in, int):
            raise ValueError("buy in must be an integer")
        elif buy_in < MINIMUM_BET:
            raise ValueError("buy in must be more than the minimum bet.")
        else:
            self.name = name
            self.buy_in = buy_in

class BlackJack:
    def __init__(self, dealer):
        self.dealer = dealer
        self.players = {"Dealer": {
                                    "cards": {},
                                    "status": "Dealing",
                                    "card_total": 0
                                    }}
        self.house_cards = {}

    
    def add_player(self, player):
        self.players[player.name] = {'total_money': player.buy_in,
                                     "cards": {},
                                     "status": 'playing',
                                     "card_total": 0
                                     } 
    

    def deal_one_card_to_players(self):
        for player, items in self.players.items():
            new_card = self.dealer.shuffled_deck[0]
            rank = new_card['rank']
            self.players[player]['cards'][rank] = new_card['suit']
            old_total = self.players[player]['card_total']
            if rank == 'J' or rank == 'Q' or rank == 'K':
                face_rank = 10
                new_total = int(face_rank) + old_total
            elif rank =="A":
                ace_rank = 11
                new_total = int(ace_rank) + old_total
            else:
                new_total = int(rank) + old_total
            self.players[player]['card_total'] = new_total
            self.dealer.shuffled_deck.pop(0)
        
    def turn(self):
        dealer_first_card = next(iter(self.players['Dealer']['cards']))
        print("dealers card: ", dealer_first_card, 'of', self.players['Dealer']['cards'][dealer_first_card])
        if dealer_first_card == 'K':
            dealer_show_card = 'King'
        elif dealer_first_card == 'Q':
            dealer_show_card = 'Queen'
        elif dealer_first_card == 'J':
            dealer_show_card = 'Jack'
        elif dealer_first_card == 'A':
            dealer_show_card = 'Ace'
        else:
            dealer_show_card = dealer_first_card
        for player, attributes in self.players.items():
            if player == 'Dealer':
                continue
            while True:
                choice = input(f"{player}, do you want to stay, hit, or double down? You currently have {attributes['card_total']}. Dealer is showing {dealer_show_card + ' of ' + self.players['Dealer']['cards'][dealer_first_card]}.")
                try:
                    if choice == 'stay':
                        break
                    elif choice == 'hit':
                        break
                    raise ValueError("Invalid input. Please enter stay, hit, or double down.")
                except ValueError as e:
                    print(e)






deck = DeckOfCards()
dealer = Dealer(deck)
Weston = Player("Weston", 300)
Rigs = Player("Rigs", 300)
game = BlackJack(dealer)
game.add_player(Weston)
game.add_player(Rigs)
# print(game.players)
game.deal_one_card_to_players()
game.deal_one_card_to_players()
print(game.players)
game.turn()