import random

class Dealer:
    def __init__(self):
        self.board = {
        00: 'green',
        0: 'green',
        1: 'red',
        2: 'red',
        3: 'black',
        4: 'red',
        5: 'black',
        6: 'black',
        7: 'red',
        8: 'red',
        9: 'black',
        10: 'red',
        11:'red',
        12:'red',
        13: 'black',
        14: 'red',
        15: 'black',
        16: 'black',
        17: 'red',
        18: 'red',
        19: 'black',
        20: 'red',
        21: 'red',
        22:'red',
        23:'red',
        24: 'black',
        25: 'red',
        26: 'black',
        27: 'black',
        28: 'red',
        29: 'red',
        30: 'black',
        31: 'red',
        32: 'black',
        33: 'black',
        34: 'red',
        35: 'red',
        36: 'black',
        }
        num, color = random.choice(list(self.board.items()))
        self.outcome = [num, color]
        half = len(self.board)/2
        self.column1 = [1,4,7,10,13,16,19,22,25,28,31,34]
        self.column2 = [2,5,8,11,14,17,20,23,26,29,32,35]
        self.column3 = [3,6,9,12,15,18,21,24,27,30,33,36]
        self.lowrange = []
        self.highrange = []
        self.first12 = []
        self.middle12 = []
        self.last12 = []
        for key, value in self.board.items():
            if key < 13 and key != 0:
                self.first12.append(key)
            if key > 12 and key < 25:
                self.middle12.append(key)
            if key > 24:
                self.last12.append(key)
            if key < half:
                self.lowrange.append(key)
            else:
                self.highrange.append(key)
        self.bets = {
                'straight': 35,
                'even': 2,
                'odd': 2,
                'black': 2,
                'red': 2,
                'lowrange': 4,
                'highrange': 4,
                'first12': 2,
                'middle12': 2,
                'last12': 2,
                'column1': 2,
                'column2': 2,
                'column3': 2,
                }

class Player: 
    def __init__(self, name=str, betType=list):
        self.betTypes = betType
        self.name = name

class Turn:
    def __init__(self, dealer=object):
        self.players = {}
        self.dealer = dealer

    def addPlayer(self, player=object):
        self.players[player.name] = player.betTypes

    def outcome(self):
        outcome = self.dealer.outcome
        payout = {}
        for key, value in self.players.items():
            for bet in value:
                if len(bet) > 2:
                    if bet[1] == outcome[0]:
                        payout[key] = (bet[2] * self.dealer.bets['straight'])
                elif bet[0] == outcome[1]:
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['red'])
                    else:
                        payout[key] * self.bets['red']
                        #this is a little funky since red and black have the same odds
                        #I figured if black=black or red=red, then the payout will be the same
                if outcome[0] //2 == 0 and bet[0] == "even":
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['even'])
                    else:
                        payout[key] * self.bets['even']
                if outcome[0] //2 == 1 and bet[0] == 'odd':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['odd'])
                    else:
                        payout[key] * self.dealer.bets['odd']
                if outcome[0] in self.dealer.lowrange and bet[0] == 'lowrange':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['lowrange'])
                    else:
                        payout[key] * self.dealer.bets['lowrange']
                if outcome[0] in self.dealer.highrange and bet[0] == 'highrange':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['highrange'])
                    else:
                        payout[key] * self.dealer.bets['highrange']
                if outcome[0] in self.dealer.first12 and bet[0] == 'first12':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['first12'])
                    else:
                        payout[key] * self.dealer.bets['first12']
                if outcome[0] in self.dealer.middle12 and bet[0] == 'middle12':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['middle12'])
                    else:
                        payout[key] * self.dealer.bets['middle12']
                if outcome[0] in self.dealer.last12 and bet[0] == 'last12':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['last12'])
                    else:
                        payout[key] * self.dealer.bets['last12']
                if outcome[0] in self.dealer.column1 and bet[0] == 'column1':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['column1'])
                    else:
                        payout[key] * self.dealer.bets['column1']
                if outcome[0] in self.dealer.column2 and bet[0] == 'column2':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['column2'])
                    else:
                        payout[key] * self.dealer.bets['column2']
                if outcome[0] in self.dealer.column3 and bet[0] == 'column3':
                    if key not in payout:
                        payout[key] = (bet[1] * self.dealer.bets['column3'])
                    else:
                        payout[key] * self.dealer.bets['column3']
                    
        print("outcome: ", outcome)
        print("payout: ", payout)






first_dealer = Dealer()
first_outcome = first_dealer.outcome
player1 = Player('player One', [["straight", 4, 100],["red", 50],['last12', 10]])
player2 = Player('player Two', [["straight", 2, 20],["even", 10]])
player3 = Player('player Three', [["black", 2000],["column1", 10]])
player4 = Player('player Four', [["column2", 500],["middle12", 1000]])
turn1 = Turn(first_dealer)
turn1.addPlayer(player1)
turn1.addPlayer(player2)
turn1.addPlayer(player3)
turn1.addPlayer(player4)
outcome1 = turn1.outcome()


### a lot of redundancy in the outcome method. Maybe create new class for payouts of
### individual bets and call functions within outcome method?
